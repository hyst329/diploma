from typing import List

import numpy as np
import pandas as pd
from base_model import ModelBase
from conv import ConvModel, ConvModelCategorical
from perceptron import PerceptronModel, PerceptronModelCategorical
from rbf import RBFModel, RBFModelCategorical
import sklearn.preprocessing as sklp
import matplotlib.pyplot as plt
from time import perf_counter

if __name__ == '__main__':
    # Load csv
    data = pd.read_csv("EURUSD_130101_180101.csv", sep=';', parse_dates=["<DATE>"])
    new_data = pd.DataFrame()
    # we need only date and close
    new_data["date"] = data["<DATE>"]
    new_data["value"] = data["<CLOSE>"]
    # log(X_n / X_{n-1})
    # new_data["log"] = sklp.scale(np.log((new_data["value"] / new_data["value"].shift(1)).fillna(1)))
    new_data["log"] = sklp.scale(new_data["value"])
    mu = np.mean(new_data["value"])
    sigma = np.std(new_data["value"])
    print(new_data["value"].describe())
    print(new_data["log"].describe())
    models: List[ModelBase] = [
        PerceptronModel((5, 20, 4), 3),
        PerceptronModel((40, 80), 20),
        PerceptronModel((40, 100, 5), 3),
        PerceptronModel((10, 20, 40, 20, 10), 8),
        PerceptronModel((250, 500), 30),
        PerceptronModel((40, 60, 30, 15), 30),
        PerceptronModelCategorical((40, 60, 30, 15), 30, 4),
        PerceptronModelCategorical((250, 500), 30, 10),
        ConvModel((250, 500), 30),
        ConvModelCategorical((250, 500), 30, 10),
        RBFModel(5, 10, 0.05),
        RBFModel(3, 60, 0.02),
        RBFModel(3, 60, 0.2),
        RBFModel(3, 60, 2),
        RBFModelCategorical(3, 60, 0.2, 4),
        RBFModelCategorical(3, 60, 0.2, 10),
    ]
    results = pd.DataFrame(columns=["model", "error", "accuracy", "r-squared", "time"])
    start = perf_counter()
    for (index, model) in enumerate(models, start=1):
        start_m = perf_counter()
        result = model.predict(new_data)
        end_m = perf_counter()
        time_m = end_m - start_m
        print(f"Model #{index}: {model} -> {result[0:2]}")
        error, accuracy = result[0] if isinstance(result[0], list) else (result[0], float("nan"))
        r_sq = result[1]
        results.loc[index] = [str(model), error, accuracy, r_sq, time_m]
        plt.cla()
        if model.is_categorical:
            t1 = plt.plot(result[2])
            t2 = plt.plot(result[3])
            plt.legend(t1 + t2, [f"Actual ({i + 1})" for i in range(model.quantiles)] +
                       [f"Predicted ({i + 1})" for i in range(model.quantiles)],
                       bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0)
            plt.title(f"RMSE = {(np.sqrt(error)):.5f} "
                      f"accuracy = {accuracy:.2%} "
                      f"R-squared = {r_sq:.2%}")
        else:
            plt.plot(result[2] * sigma + mu, label="Actual")
            plt.plot(result[3] * sigma + mu, label="Predicted")
            plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0)
            plt.title(f"RMSE = {(np.sqrt(error) * sigma):.5f} "
                      f"R-squared = {r_sq:.2%}")
        plt.suptitle(str(model))
        plt.savefig(f"plots/{index}.eps", bbox_inches="tight")
    end = perf_counter()
    exec_time = end - start
    print(results)
    print(f"Execution time (seconds): {exec_time} ({exec_time / len(models)} averaged per model)")
    results.to_csv("results.csv")
