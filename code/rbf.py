from keras.layers import Dense
from keras.models import Sequential

from base_model import ModelBase
from rbflayer import RBFLayer, InitCentersRandom
import pandas as pd
import numpy as np


class RBFModel(ModelBase):
    def __init__(self, win_size: int, units: int, betas: float, column_name: str = "log"):
        self.win_size = win_size
        self.betas = betas
        self.units = units
        super().__init__(column_name)

    def predict(self, data: pd.DataFrame):
        series_x, series_y = self.build_dataset(data[self.column_name].values, self.win_size)
        test_count = int(np.round(len(series_x) * self.TEST_FRACTION))
        x_train, x_test = series_x[:-test_count], series_x[-test_count:]
        y_train, y_test = series_y[:-test_count], series_y[-test_count:]
        model = Sequential()
        model.add(RBFLayer(self.units,
                           initializer=InitCentersRandom(x_train),
                           betas=self.betas,
                           input_shape=(self.win_size,)))
        model.add(Dense(1, activation='linear'))  # output layer
        model.compile(loss='mse', optimizer='adam')
        model.fit(x_train, y_train, epochs=160)
        y_pred = model.predict(x_test)
        return model.evaluate(x_test, y_test, verbose=0), self.coeff_determination(y_test, y_pred), y_test, y_pred

    def __str__(self):
        return f"RBF ({self.win_size}, {self.units}, {self.quantiles})"

    @property
    def is_categorical(self) -> bool:
        return False

    @property
    def quantiles(self):
        return 1


class RBFModelCategorical(ModelBase):
    def __init__(self, win_size: int, units: int, betas: float, quantiles: int, column_name: str = "log"):
        self.win_size = win_size
        self.betas = betas
        self.units = units
        self._quantiles = quantiles
        super().__init__(column_name)

    def predict(self, data: pd.DataFrame):
        series_x, series_y = self.build_dataset(data[self.column_name].values, self.win_size)
        test_count = int(np.round(len(series_x) * self.TEST_FRACTION))
        x_train, x_test = series_x[:-test_count], series_x[-test_count:]
        y_train, y_test = series_y[:-test_count], series_y[-test_count:]
        model = Sequential()
        model.add(RBFLayer(self.units,
                           initializer=InitCentersRandom(x_train),
                           betas=self.betas,
                           input_shape=(self.win_size,)))
        model.add(Dense(self.quantiles, activation='linear'))  # output layer
        model.compile(loss='mse',
                      optimizer='adam',
                      metrics=['accuracy'])
        model.fit(x_train, y_train, epochs=160)
        y_pred = model.predict(x_test)
        return model.evaluate(x_test, y_test, verbose=0), self.coeff_determination(y_test, y_pred), y_test, y_pred

    def __str__(self):
        return f"RBF (categorical) ({self.win_size}, {self.units}, {self.quantiles})"

    @property
    def is_categorical(self) -> bool:
        return True

    @property
    def quantiles(self):
        return self._quantiles
