from keras import Sequential
from keras.layers import Dense, Conv1D, Flatten, AvgPool1D

from base_model import ModelBase
import pandas as pd
import numpy as np


class ConvModel(ModelBase):

    def __init__(self, layer_config: tuple, win_size: int, column_name: str = "log"):
        self.win_size = win_size
        self.layer_config = layer_config
        super().__init__(column_name)

    def predict(self, data: pd.DataFrame):
        def create_model():
            model = Sequential()
            model.add(Conv1D(self.layer_config[0], 2,
                            input_shape=(self.win_size, 1),
                            kernel_initializer='normal',
                             padding='causal',
                            activation='relu'
                            ))  # input layer
            model.add(AvgPool1D(pool_size=2))
            model.add(Flatten())
            for layer in self.layer_config:
                # model.add(Dropout(0.25))
                model.add(
                    Dense(layer,
                          kernel_initializer='normal',
                          activation='relu'
                          ))
            model.add(
                Dense(1, kernel_initializer='normal', activation='linear'))  # output layer
            model.compile(loss='mse',
                          optimizer='adam')
            return model

        model = create_model()
        series_x, series_y = self.build_dataset(data[self.column_name].values, self.win_size)
        test_count = int(np.round(len(series_x) * self.TEST_FRACTION))
        x_train, x_test = series_x[:-test_count], series_x[-test_count:]
        y_train, y_test = series_y[:-test_count], series_y[-test_count:]
        model.fit(np.expand_dims(x_train, axis=2), y_train, epochs=32, batch_size=100)
        y_pred = model.predict(np.expand_dims(x_test, axis=2))
        # estimator = KerasClassifier(build_fn=create_model, epochs=32, batch_size=5, verbose=0)
        # kfold = StratifiedKFold(n_splits=4, shuffle=True)
        # results = cross_val_score(estimator, series_x, series_y, cv=kfold)
        # return results.mean(), results.std()
        return model.evaluate(np.expand_dims(x_test, axis=2), y_test, verbose=0), self.coeff_determination(y_test, y_pred), y_test, y_pred

    def __str__(self):
        return f"Convolutional NN {(self.win_size, *self.layer_config, 1)}"

    @property
    def is_categorical(self) -> bool:
        return False

    @property
    def quantiles(self):
        return 1


class ConvModelCategorical(ModelBase):
    def __init__(self, layer_config: tuple, win_size: int, quantiles: int, column_name: str = "log"):
        self.win_size = win_size
        self.layer_config = layer_config
        self._quantiles = quantiles
        super().__init__(column_name)

    def predict(self, data: pd.DataFrame):
        def create_model():
            model = Sequential()
            model.add(Conv1D(self.layer_config[0], 2,
                             input_shape=(self.win_size, 1),
                             kernel_initializer='normal',
                             padding='causal',
                             activation='relu'
                             ))  # input layer
            model.add(AvgPool1D(pool_size=2))
            model.add(Flatten())
            for layer in self.layer_config:
                # model.add(Dropout(0.25))
                model.add(
                    Dense(layer,
                          kernel_initializer='normal',
                          activation='relu'
                          ))
            model.add(
                Dense(self.quantiles, kernel_initializer='normal', activation='linear'))  # output layer
            model.compile(loss='mse',
                          optimizer='adam',
                          metrics=['accuracy'])
            return model

        model = create_model()
        series_x, series_y = self.build_dataset(data[self.column_name].values, self.win_size)
        test_count = int(np.round(len(series_x) * self.TEST_FRACTION))
        x_train, x_test = series_x[:-test_count], series_x[-test_count:]
        y_train, y_test = series_y[:-test_count], series_y[-test_count:]
        sample_weights = np.linspace(0, 1, len(x_train)) ** 0.5
        model.fit(np.expand_dims(x_train, axis=2), y_train, epochs=32, batch_size=100, sample_weight=sample_weights)
        y_pred = model.predict(np.expand_dims(x_test, axis=2))
        # estimator = KerasClassifier(build_fn=create_model, epochs=32, batch_size=5, verbose=0)
        # kfold = StratifiedKFold(n_splits=4, shuffle=True)
        # results = cross_val_score(estimator, series_x, series_y, cv=kfold)
        # return results.mean(), results.std()
        return model.evaluate(np.expand_dims(x_test, axis=2), y_test, verbose=0), self.coeff_determination(y_test, y_pred), y_test, y_pred

    def __str__(self):
        return f"Convolutional NN (cat.) {(self.win_size, *self.layer_config, self.quantiles)}"

    @property
    def is_categorical(self) -> bool:
        return True

    @property
    def quantiles(self):
        return self._quantiles