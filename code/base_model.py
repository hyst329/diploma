from abc import ABC, abstractmethod
import pandas as pd
import numpy as np
import scipy.stats as sps
import keras.utils.np_utils as knpu


class ModelBase(ABC):
    TEST_FRACTION = 0.2  # 10% test, 90% training

    def __init__(self, column_name: str = "log"):
        self.column_name = column_name
        pass

    @abstractmethod
    def predict(self, data: pd.DataFrame):
        pass

    def build_dataset(self, dataset: np.ndarray, win_size: int):
        data_x, data_y = [], []
        for i in range(len(dataset) - win_size - 1):
            a = dataset[i:(i + win_size)]
            data_x.append(a)
            if self.is_categorical:
                b = int(sps.percentileofscore(dataset, dataset[i + win_size]) / 100 * self.quantiles)
                data_y.append(min(b, self.quantiles - 1))
            else:
                b = dataset[i + win_size]
                data_y.append(b)
        if self.is_categorical:
            data_y = knpu.to_categorical(data_y, num_classes=self.quantiles)
        return np.array(data_x), np.array(data_y)

    @property
    @abstractmethod
    def is_categorical(self) -> bool:
        pass

    @property
    @abstractmethod
    def quantiles(self):
        return 0

    @staticmethod
    def coeff_determination(y_true, y_pred):
        y_true = np.ravel(y_true)
        y_pred = np.ravel(y_pred)
        SS_res = np.sum(np.square(y_true - y_pred))
        SS_tot = np.sum(np.square(y_true - np.mean(y_true)))
        return 1 - SS_res / (SS_tot + 1e-12)

